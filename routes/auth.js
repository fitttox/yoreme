// Rutas para autenticar usuarios
const express = require('express');
const { check } = require('express-validator');
const auth = require('../midd/auth');
const { authUser, userNext } = require('../controler/authController');
const router = express.Router();

// Iniciar sesión
// api/auth
router.post('/',
    authUser
);

// Obtiene el usuario autenticado
router.get('/',
    auth,
    userNext
);
module.exports = router;