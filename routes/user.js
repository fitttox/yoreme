const express = require("express");
const { check } = require("express-validator");
const { celularUser1, celularUser, registerUser, actualizarUser, photoUser, obtenerUserPhoto, obtenerUser,
    registerUserNative, actualizarUserPass } = require("../controler/userController");
const auth = require("../midd/auth");
const multipart = require("connect-multiparty");
const part = multipart({ uploadDir: './uploads/users' })
const ruta = express.Router();


// @route    GET api/auth
// @desc     Get user by token
// @access   Private
ruta.post("/",
    registerUser);
ruta.post("/celular",
    celularUser);
ruta.post("/celular1",
    celularUser1);
ruta.put("/:id",
    auth,
    actualizarUser);
ruta.put("/",
    actualizarUserPass);
ruta.post("/photo/:id",
    [
        auth,
        part
    ],
    photoUser);
ruta.get("/:photo", obtenerUserPhoto);
ruta.get("/f/:id", obtenerUser);
// ruta.get("/r", preRegistrarUser);
//native
ruta.post("/native/", registerUserNative);
module.exports = ruta;