const express = require("express");
const { check } = require("express-validator");
const { postJoaFoto,
    obtenerJoaPhoto,
    postJoa,
    putJoa,
    deleteJoa,
    getJoaAll,
    obtenerJoa,
    joaLike,
    joaDislike,
    comentarJoa,
    descomentarJoa } = require("../controler/joaController");
const auth = require("../midd/auth");
const multipart = require("connect-multiparty");
const part = multipart({ uploadDir: './uploads/joa' })
const ruta = express.Router();

ruta.post("/photo/:id",
    [auth, part],
    postJoaFoto);

ruta.get("/:photo", obtenerJoaPhoto);

ruta.post("/:id", [auth, [
    check("nombre", "Falta nombre").not().isEmpty(),
    check("texto", "No la descripcion").not().isEmpty(),
]], postJoa);
//id de la compania 
ruta.put("/:id", auth, putJoa);

ruta.delete("/:id", auth, deleteJoa);

ruta.get('/', getJoaAll);

ruta.get('/all/:id', auth, obtenerJoa);

ruta.put('/like/:id', auth, joaLike);

ruta.put('/unlike/:id', auth, joaDislike);

ruta.post('/comentar/:id',
    [
        auth,
        [check('text', 'El texto es necesario').not().isEmpty()]
    ], comentarJoa
);

ruta.delete('/comentar/:id/:comment_id', auth, descomentarJoa);

module.exports = ruta;