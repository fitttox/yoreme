const express = require("express");
const { check } = require("express-validator");
const { postProductoFoto,
    obtenerPhoto,
    postProducto,
    putProducto,
    deleteProducto,
    obtenerProducto,
    obtenerTodo,
    darLike,
    quitarLike,
    comentarProducto,
    quitarComentado,
    todoPublico } = require("../controler/productoController");
const auth = require("../midd/auth");
const idCheck = require("../midd/idCheck");
const multipart = require("connect-multiparty");
const part = multipart({ uploadDir: './uploads/productos' })
const ruta = express.Router();

ruta.post("/photo/:id",
    [auth, part],
    postProductoFoto);

ruta.get("/:photo", obtenerPhoto);

ruta.post("/:id", [auth, [
    check("nombre", "Falta nombre").not().isEmpty(),
    check("precio", "No olvide el precio").not().isEmpty(),
]], postProducto);

ruta.put("/:id", auth, putProducto);

ruta.delete("/:id", auth, deleteProducto);

ruta.get('/', obtenerTodo);

ruta.get('/all/:id', [auth, idCheck('id')], obtenerProducto);

ruta.put('/like/:id', [auth, idCheck('id')], darLike);

ruta.put('/unlike/:id', [auth, idCheck('id')], quitarLike);

ruta.post('/comentar/:id',
    [
        auth,
        idCheck('id'),
        [check('text', 'El texto es necesario').not().isEmpty()]
    ], comentarProducto
);

ruta.delete('/comentar/:id/:comment_id', auth, quitarComentado);



module.exports = ruta;