const User = require('../model/User');
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');

exports.authUser = async (req, res) => {
    // extraer el email y password
    let { celular, password } = req.body;
    try {
        // Revisar que sea un usuario registrado
        let user = await User.findOne({ celular });
        if (!user) return res.status(400).json({ msg: 'El usuario no existe' });
        // Revisar el password
        const passCorrecto = await bcryptjs.compare(password, user.password);
        if (!passCorrecto) return res.status(400).json({ msg: 'Password Incorrecto' })
        // Si todo es correcto Crear y firmar el JWT
        const payload = {
            user: {
                id: user.id
            }
        };
        // firmar el JWT
        jwt.sign(payload, process.env.JWT_SECRETO, {
            expiresIn: 3600 // 1 hora
        }, (error, token) => {
            if (error) throw error;
            // Mensaje de confirmación
            res.json({ token });
        });
    } catch (error) {
        console.log(error);
    }
}
// Obtiene que usuario esta autenticado
exports.userNext = async (req, res) => {
    try {
        const user = await User.findById(req.user.id).select('-password');
        res.json({ user });
    } catch (error) {
        console.log(error);
        res.status(500).json({ msg: 'Hubo un error' });
    }
    //espale
}