const User = require("../model/User");
const bcryptjs = require('bcryptjs');
const { validationResult } = require("express-validator");
const jwt = require('jsonwebtoken');
const fs = require("fs");
const path = require("path");
require('dotenv').config({ path: 'variables.env' });


exports.registerUser = async (req, res) => {
    const errores = validationResult(req);
    if (!errores.isEmpty()) {
        return res.status(400).json({ errores: errores.array() });
    }
    let { celular, password, name } = req.body;
    try {
        let user = await User.findOne({ celular });
        if (user) return res.status(400).json({ msg: 'Ya existe # celular' });
        if (!user) {
            user = new User({ celular, password, name, })
            // Hashear el password
            const salt = await bcryptjs.genSalt(10);
            user.password = await bcryptjs.hash(password, salt);
            user.name = name;
            user.photo = null;
            await user.save();
            // //antes de mande
            // client.messages.create({
            //     from: 'whatsapp:+14155238886',
            //     body: `${name}, su clave de acceso es ${req.body.password}, con duracion de una hora.`,
            //     to: `whatsapp:+521${celular}`
            // });
            const payload = {
                user: {
                    id: user.id,
                },
            };
            jwt.sign(payload, process.env.JWT_SECRETO, {
                expiresIn: 360000 /* 3600 = 1hora */
            }, (err, token) => {
                if (err) throw err;
                return res.json({ token });
            });
        }
    } catch (error) {
        console.log(error);
        res.status(400).json({ error: "Tenemos un problema. RegisterUser 60" });
    }
}
exports.celularUser = async (req, res) => {
    let { celular } = req.body;
    try {
        let user = await User.findOne({ celular });
        if (user) return res.status(400).json({ msg: 'Ya existe # celular' });
        return res.json({ msg: 'Todo bien' });
    } catch (error) {
        console.log(error);
        res.status(400).json({ error: "Tenemos un problema. RegisterUser 60" });
    }
}
exports.celularUser1 = async (req, res) => {
    let { celular } = req.body;
    try {
        let user = await User.findOne({ celular });
        if (!user) return res.status(400).json({ msg: 'No existe # celular' });
        return res.json({ msg: 'Todo bien' });
    } catch (error) {
        console.log(error);
        res.status(400).json({ error: "Tenemos un problema. RegisterUser 60" });
    }
}
exports.actualizarUserPass = async (req, res) => {
    const errores = validationResult(req);
    if (!errores.isEmpty()) {
        return res.status(400).json({ errores: errores.array() });
    }
    // extraer la información del proyecto
    let { celular, password } = req.body;
    try {
        let cel = await User.findOne({ celular });
        if (!cel) return res.status(400).json({ msg: 'No existe # celular' });
        if (password) {
            const salt = await bcryptjs.genSalt(10);
            const word = await bcryptjs.hash(password, salt);
            let user = await User.findByIdAndUpdate(cel._id, { password: word }, { new: true });
            res.json({ user })
            // let user = await User.findOneAndUpdate()
        }
        // if (!cel) {
        //     if (password) {
        //         // revisar el ID 
        //         let user = await User.findByIdAndUpdate(req.params.id, req.body, { new: true }).select('-password');
        //         // si el proyecto existe o no
        //         if (!user) return res.status(404).json({ msg: 'Ocurrio un error' });
        //         res.json({ user });
        //     } else {
        //         return res.status(200).json({ msg: 'Ocurrio un error' })
        //     }
        // } 
    } catch (error) {
        console.log(error);
        res.status(500).send('Error en el servidor');
    }
}
exports.actualizarUser = async (req, res) => {
    // revisar si hay errores
    const errores = validationResult(req);
    if (!errores.isEmpty()) {
        return res.status(400).json({ errores: errores.array() });
    }

    // extraer la información del proyecto
    let { name, celular, photo } = req.body;
    try {
        let cel = await User.findOne({ celular });
        if (!cel) {
            if (name || celular || photo) {
                // revisar el ID 
                let user = await User.findByIdAndUpdate(req.params.id, req.body, { new: true }).select('-password');
                // si el proyecto existe o no
                if (!user) return res.status(404).json({ msg: 'Ocurrio un error' });
                res.json({ user });
            } else {
                return res.status(200).json({ msg: 'Ocurrio un error' })
            }
        } else {
            return res.status(400).json({ msg: 'Ya existe # celular' });
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error en el servidor');
    }
}
exports.photoUser = async (req, res) => {
    // extraer la información del proyecto
    let body = req.body.otra;
    if (req.files) {
        if (body !== null && body !== undefined) {
            let otra_split = body.split('\/');
            let otra_name = otra_split[5];
            let path_file = `./uploads/users/${otra_name}`;

            try {
                fs.unlink(path_file, (err) => {
                    console.log("err", err);
                });
            } catch (error) {
                return res.status(200).json({ msg: 'Ocurrio un error' })
            }
        }
        let file_path = req.files.photo.path;
        let file_split = file_path.split('\/');
        let file_name = file_split[2];

        let ext_split = file_name.split('\.');
        let file_ext = ext_split[1];

        try {
            if (file_ext === 'png' ||
                file_ext === 'jpg' ||
                file_ext === 'jpeg' ||
                file_ext === 'gif') {

                res.json({ file_name });
            } else {
                fs.unlink(file_path, err => {
                    if (err) {
                        return res.status(200).json({ msg: 'Ocurrio un error' })
                    } else {
                        return res.status(200).json({ msg: 'Ocurrio un error' })
                    }
                });
                return res.status(200).json({ msg: 'Ocurrio un error' })
            }
        } catch (error) {
            console.log(error);
            res.status(500).send('Error en el servidor');
        }
    } else {
        return res.status(404).json({ msg: 'Ocurrio un error' })
    }
}
exports.obtenerUserPhoto = async (req, res) => {
    // extraer la información del proyecto
    let path_file = `./uploads/users/${req.params.photo}`;
    try {
        fs.exists(path_file, await (exists => {
            if (exists) {
                res.sendFile(path.resolve(path_file));
            } else {
                return res.status(200).json({ msg: 'Ocurrio un error' })
            }
        }));
    } catch (error) {
        console.log(error);
        res.status(500).send('Error en el servidor');
    }

}
exports.obtenerUser = async (req, res) => {
    const { id } = req.params;
    try {
        let user = await User.findById(id);
        if (!user) {
            return res.status(400).json({ msg: 'No existe # celular' });
        } else {
            return res.json(user);
        }
    } catch (error) {
        console.log(error);
        res.status(400).json({ error: "Tenemos un problema. Encuesta Control" });
    }
}
// exports.obtenerPass = async (req, res) => {
//     const { id } = req.params;
//     try {
//         let user = await User.findOne(id);
//         if (!user) return res.status(400).json({ msg: 'No existe # celular' });
//         const salt = await bcryptjs.genSalt(10);
//         let password = await bcryptjs.compare(id, salt);
//         return res.json(password);
//         // } else {
//         //     
//         // }
//     } catch (error) {
//         console.log(error);
//         res.status(400).json({ error: "Tenemos un problema. Encuesta Control" });
//     }
// }
//native

exports.registerUserNative = async (req, res) => {
    console.log("native body", req);
    const { celular, password } = req.body;
    console.log("native body", celular, password);
    try {
        let user = await User.findOne({ celular });
        if (!user) {
            user = new User({
                celular,
                password
            })
            //Hashear el password
            const salt = await bcryptjs.genSalt(10);
            user.password = await bcryptjs.hash(password, salt);
            user.name = null;
            user.photo = null;

            await user.save();
            const payload = {
                user: {
                    id: user.id,
                },
            };

            jwt.sign(payload, process.env.JWT_SECRETO, {
                expiresIn: 360000 /* 3600 = 1hora */
            }, (err, token) => {
                if (err) throw err;
                return res.json({ token });
            });
        } else {
            return res.status(400).json({ msg: 'Ya existe # celular' });
        }
    } catch (error) {
        console.log(error);
        res.status(400).json({ error: "Tenemos un problema. Encuesta Control" });
    }
}