const Producto = require("../model/Producto");
const User = require("../model/User");
const { validationResult } = require("express-validator");
const fs = require("fs");
const path = require("path");
require('dotenv').config({ path: 'variables.env' });

exports.postProductoFoto = async (req, res) => {
    // extraer la información del proyecto
    let body = req.body.otra;
    if (req.files) {
        if (body !== null && body !== undefined) {
            let otra_split = body.split('\/');
            let otra_name = otra_split[5];
            let path_file = `./uploads/z/${otra_name}`;

            try {
                fs.unlink(path_file, (err) => {
                    console.log("err", err);
                });
            } catch (error) {
                return res.status(200).json({ msg: 'Ocurrio un error' })
            }
        }
        let file_path = req.files.photo.path;
        let file_split = file_path.split('\/');
        let file_name = file_split[2];

        let ext_split = file_name.split('\.');
        let file_ext = ext_split[1];

        try {
            if (file_ext === 'png' ||
                file_ext === 'jpg' ||
                file_ext === 'jpeg' ||
                file_ext === 'gif') {
                res.json({ file_name });
            } else {
                fs.unlink(file_path, err => {
                    if (err) {
                        return res.status(200).json({ msg: 'Ocurrio un error' })
                    } else {
                        return res.status(200).json({ msg: 'Ocurrio un error' })
                    }
                });
                return res.status(200).json({ msg: 'Ocurrio un error' })
            }
        } catch (error) {
            console.log(error);
            res.status(500).send('Error en el servidor');
        }
    } else {
        return res.status(404).json({ msg: 'Ocurrio un error' })
    }
}
exports.obtenerPhoto = async (req, res) => {
    // extraer la información del proyecto
    let path_file = `./uploads/productos/${req.params.photo}`;
    try {
        fs.exists(path_file, await (exists => {
            if (exists) {
                res.sendFile(path.resolve(path_file));
            } else {
                return res.status(200).json({ msg: 'Ocurrio un error' })
            }
        }));
    } catch (error) {
        console.log(error);
        res.status(500).send('Error en el servidor');
    }
}
exports.postProducto = async (req, res) => {
    // revisar si hay errores
    const errores = validationResult(req);
    if (!errores.isEmpty()) {
        return res.status(400).json({ errores: errores.array() });
    }

    const { photo, nombre, precio, texto, mostrar } = req.body;

    try {
        // const user = await User.findById(req.user.id).select('-password');

        const newProducto = new Producto({
            user: req.user.id,
            image: photo,
            nombre: nombre,
            precio: precio,
            texto: texto,
            mostrar: true
        });

        const producto = await newProducto.save();

        res.json(producto);
    } catch (error) {
        console.log(error);
        res.status(500).send('Error en el servidor');
    }
}
exports.putProducto = async (req, res) => {
    // extraer la información del proyecto
    const { image, nombre, precio, texto, mostrar } = req.body;
    console.log("putProducto 1", nombre, precio, image, texto, mostrar);
    try {
        if (nombre || precio || image || texto || mostrar) {
            console.log("putProducto 2", nombre, precio, image, texto, mostrar);
            // revisar el ID 
            let producto = await Producto.findByIdAndUpdate(req.params.id, req.body, { new: true });
            // si el proyecto existe o no
            if (!producto) return res.status(404).json({ msg: 'Ocurrio un error' });
            res.json({ producto });
        } else {
            return res.status(200).json({ msg: 'Ocurrio un error' })
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error en el servidor');
    }
}
exports.deleteProducto = async (req, res) => {
    try {
        const producto = await Producto.findById(req.params.id);

        if (!producto) {
            return res.status(404).json({ msg: 'Producto no encontrado' });
        }

        // Check user
        if (producto.user.toString() !== req.user.id) {
            return res.status(401).json({ msg: 'Usuario no autorizado' });
        }
        await producto.remove();

        res.json({ msg: 'Producto removido' });
    } catch (err) {
        console.error(err.message);

        res.status(500).send('Server Error');
    }
}
exports.obtenerTodo = async (req, res) => {
    try {
        const producto = await Producto.find().sort({ date: -1 });
        res.json(producto);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
}
exports.obtenerProducto = async (req, res) => {
    try {
        const producto = await Producto.findById(req.params.id);

        if (!producto) {
            return res.status(404).json({ msg: 'Producto no encontrad' })
        }

        res.json(producto);
    } catch (err) {
        console.error(err.message);

        res.status(500).send('Server Error');
    }
}
exports.darLike = async (req, res) => {
    try {
        const producto = await Producto.findById(req.params.id);

        // Check if the post has already been liked
        if (producto.likes.some(like => like.user.toString() === req.user.id)) {
            return res.status(400).json({ msg: 'Te emocionaste?' });
        }

        producto.likes.unshift({ user: req.user.id });

        await producto.save();

        return res.json(producto.likes);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
}
exports.quitarLike = async (req, res) => {
    try {
        const producto = await Producto.findById(req.params.id);

        // Check if the post has not yet been liked
        if (!producto.likes.some(like => like.user.toString() === req.user.id)) {
            return res.status(400).json({ msg: 'No tiene likes' });
        }

        // remove the like
        producto.likes = producto.likes.filter(
            ({ user }) => user.toString() !== req.user.id
        );

        await producto.save();

        return res.json(producto.likes);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
}
exports.comentarProducto = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    try {
        const user = await User.findById(req.user.id).select('-password');
        const producto = await Producto.findById(req.params.id);

        const newComment = {
            text: req.body.text,
            nombre: user.name,
            foto: user.photo,
            user: req.user.id
        };

        producto.comments.unshift(newComment);

        await producto.save();

        res.json(producto.comments);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
}
exports.quitarComentado = async (req, res) => {
    try {
        const producto = await Producto.findById(req.params.id);

        // Pull out comment
        const comment = producto.comments.find(
            comment => comment.id === req.params.comment_id
        );
        // Make sure comment exists
        if (!comment) {
            return res.status(404).json({ msg: 'No existe el comentario' });
        }
        // Check user
        if (comment.user.toString() !== req.user.id) {
            return res.status(401).json({ msg: 'No autorizado' });
        }

        producto.comments = producto.comments.filter(
            ({ id }) => id !== req.params.comment_id
        );

        await producto.save();

        return res.json(producto.comments);
    } catch (err) {
        console.error(err.message);
        return res.status(500).send('Server Error');
    }
}