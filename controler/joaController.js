const Compania = require('../model/Compania');
const User = require("../model/User");
const { validationResult } = require("express-validator");
const fs = require("fs");
const path = require("path");
require('dotenv').config({ path: 'variables.env' });

exports.postJoaFoto = async (req, res) => {
    // extraer la información del proyecto
    let body = req.body.otra;
    if (req.files) {
        if (body !== null && body !== undefined) {
            let otra_split = body.split('\/');
            let otra_name = otra_split[5];
            let path_file = `./uploads/joa/${otra_name}`;

            try {
                fs.unlink(path_file, (err) => {
                    console.log("err", err);
                });
            } catch (error) {
                return res.status(200).json({ msg: 'Ocurrio un error' })
            }
        }
        let file_path = req.files.photo.path;
        let file_split = file_path.split('\/');
        let file_name = file_split[2];

        let ext_split = file_name.split('\.');
        let file_ext = ext_split[1];

        try {
            if (file_ext === 'png' ||
                file_ext === 'jpg' ||
                file_ext === 'jpeg' ||
                file_ext === 'gif') {
                res.json({ file_name });
            } else {
                fs.unlink(file_path, err => {
                    if (err) {
                        return res.status(200).json({ msg: 'Ocurrio un error' })
                    } else {
                        return res.status(200).json({ msg: 'Ocurrio un error' })
                    }
                });
                return res.status(200).json({ msg: 'Ocurrio un error' })
            }
        } catch (error) {
            console.log(error);
            res.status(500).send('Error en el servidor');
        }
    } else {
        return res.status(404).json({ msg: 'Ocurrio un error' })
    }
}
exports.obtenerJoaPhoto = async (req, res) => {
    // extraer la información del proyecto
    let path_file = `./uploads/joa/${req.params.photo}`;
    try {
        fs.exists(path_file, await (exists => {
            if (exists) {
                res.sendFile(path.resolve(path_file));
            } else {
                return res.status(200).json({ msg: 'Ocurrio un error' })
            }
        }));
    } catch (error) {
        console.log(error);
        res.status(500).send('Error en el servidor');
    }//algo
}
exports.postJoa = async (req, res) => {
    // revisar si hay errores
    const errores = validationResult(req);
    if (!errores.isEmpty()) {
        return res.status(400).json({ errores: errores.array() });
    }

    const { photo, nombre, texto } = req.body;

    try {
        const newJoa = new Compania({
            user: req.user.id,
            image: photo,
            nombre: nombre,
            texto: texto,
        });

        const joa = await newJoa.save();

        res.json(joa);
    } catch (error) {
        console.log(error);
        res.status(500).send('Error en el servidor');
    }
}
exports.putJoa = async (req, res) => {
    // extraer la información del proyecto
    const { image, nombre, texto } = req.body;
    try {
        if (nombre || image || texto) {
            // revisar el ID 
            let joa = await Compania.findByIdAndUpdate(req.params.id, req.body, { new: true });
            console.log("joa", joa);
            // si el joa existe o no
            if (!joa) return res.status(404).json({ msg: 'Ocurrio un error' });
            res.json({ joa });
        } else {
            return res.status(200).json({ msg: 'Ocurrio un error' })
        }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error en el servidor');
    }
}
exports.deleteJoa = async (req, res) => {
    try {
        const joa = await Compania.findById(req.params.id);

        if (!joa) {
            return res.status(404).json({ msg: 'Joa no encontrado' });
        }

        // Check user
        if (joa.user.toString() !== req.user.id) {
            return res.status(401).json({ msg: 'Usuario no autorizado' });
        }
        await joa.remove();

        res.json({ msg: 'Joa removide' });
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
}
exports.getJoaAll = async (req, res) => {
    try {
        const joa = await Compania.find().sort({ date: -1 });
        res.json(joa);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
}
exports.obtenerJoa = async (req, res) => {
    try {
        const joa = await Compania.findById(req.params.id);

        if (!joa) {
            return res.status(404).json({ msg: 'Joa no encontrade' })
        }

        res.json(joa);
    } catch (err) {
        console.error(err.message);

        res.status(500).send('Server Error');
    }
}
exports.joaLike = async (req, res) => {
    try {
        const joa = await Compania.findById(req.params.id);

        // Check if the post has already been liked
        if (joa.likes.some(like => like.user.toString() === req.user.id)) {
            return res.status(400).json({ msg: 'Te emocionaste?' });
        }

        joa.likes.unshift({ user: req.user.id });

        await joa.save();

        return res.json(joa.likes);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
}
exports.joaDislike = async (req, res) => {
    try {
        const joa = await Compania.findById(req.params.id);

        // Check if the post has not yet been liked
        if (!joa.likes.some(like => like.user.toString() === req.user.id)) {
            return res.status(400).json({ msg: 'No tiene likes' });
        }

        // remove the like
        joa.likes = joa.likes.filter(
            ({ user }) => user.toString() !== req.user.id
        );

        await joa.save();

        return res.json(joa.likes);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
}
exports.comentarJoa = async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    try {
        const user = await User.findById(req.user.id).select('-password');
        const joa = await Compania.findById(req.params.id);
        const newComment = {
            text: req.body.text,
            nombre: user.name,
            foto: user.photo.url,
            user: req.user.id
        };

        joa.comments.unshift(newComment);

        await joa.save();

        res.json(joa.comments);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
}
exports.descomentarJoa = async (req, res) => {
    try {
        // const joa = await Compania.findById(req.params.id);
        console.log("params", { params: req.params })
        // const buscarJoa = await Compania.findById(req.params.comment_id);
        const joa = await Compania.findById(req.params.id);
        if (!joa) {
            return res.status(404).json({ msg: "No existe el comentario" });

        }
        // console.log("joa", joa);

        // Pull out comment
        const comment = joa.comments.find(
            comment => comment.id === req.params.comment_id
        );
        // Make sure comment exists
        if (!comment) {
            return res.status(404).json({ msg: 'No existe el comentario' });
        }
        console.log("comment", comment);
        // Check user
        if (comment.user.toString() !== req.user.id) {
            return res.status(401).json({ msg: 'No autorizado' });
        }
        joa.comments = joa.comments.filter(
            ({ id }) => id !== req.params.comment_id
        );

        await joa.save();

        return res.json(joa.comments);
    } catch (err) {
        console.error(err.message);
        return res.status(500).send('Server Error');
    }
}