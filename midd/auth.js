const jwt = require("jsonwebtoken");
require("dotenv").config({ path: "variables.env" });

module.exports = function (req, res, next) {
    // Leer el token del header
    const token = req.header("x-token");

    // Revisar si hay token
    if (!token) {
        return res.status(401).json({ msg: "Acceso no autorizado" });
    }
    // Verify token
    try {
        jwt.verify(token, process.env.JWT_SECRETO, (error, decoded) => {
            if (error) {
                res.status(401).json({ msg: "Token no valido" });
            } else {
                req.user = decoded.user;
                next();
            }
        });
    } catch (err) {
        console.error("algo paso mal con el middleware");
        res.status(500).json({ msg: "Error del Servidor" });
    }
};