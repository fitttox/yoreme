const mongoose = require('mongoose');
// middleware to check for a valid object id
const idCheck = (idToCheck) => (req, res, next) => {
    if (!mongoose.Types.ObjectId.isValid(req.params[idToCheck]))
        return res.status(400).json({ msg: 'ID invalido' });
    next();
};

module.exports = idCheck;