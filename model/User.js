const mongoose = require("mongoose");

const UserSchema = mongoose.Schema({
    celular: {
        type: String,
    },
    password: {
        type: String,
    },
    name: {
        type: String,
    },
    photo: {
        type: Object,
    },
    fecha: {
        type: Date,
        default: Date.now(),
    },
});

module.exports = mongoose.model("User", UserSchema);