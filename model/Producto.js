const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductoSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    image: {
        type: Object
    },
    nombre: {
        type: String
    },
    precio: {
        type: Number,
    },
    texto: {
        type: String,
    },
    mostrar: {
        type: Boolean,
    },
    material: {
        type: Schema.Types.ObjectId,
        ref: 'Material'
    },
    likes: [
        {
            user: {
                type: Schema.Types.ObjectId,
                ref: 'User'
            }
        }
    ],
    comments: [
        {
            user: {
                type: Schema.Types.ObjectId,
                ref: 'User'
            },
            text: {
                type: String
            },
            nombre: {
                type: String
            },
            foto: {
                type: String
            },
            fecha: {
                type: Date,
                default: Date.now(),
            },
        }
    ],
    fecha: {
        type: Date,
        default: Date.now(),
    },
});

module.exports = mongoose.model('Producto', ProductoSchema);